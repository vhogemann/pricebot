package com.hogemann.pricebot.provider;

public interface Parser<I,O> {

	O parse(I input);
	
}
