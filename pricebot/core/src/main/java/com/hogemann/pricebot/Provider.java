package com.hogemann.pricebot;

import java.util.List;

import com.hogemann.pricebot.model.Product;

public interface Provider<P extends Product> {

	List<P> find(String ... ids );
	
}
