package com.hogemann.pricebot.model;

import java.math.BigDecimal;

import com.hogemann.pricebot.Provider;

public class Product extends AbstractEntity {

	private static final long serialVersionUID = 1L;

	private String name;

	private String brand;

	private String description;

	private BigDecimal price;
	
	private String code;

	private Provider<Product> provider;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Provider<Product> getProvider() {
		return provider;
	}

	public void setProvider(Provider<Product> provider) {
		this.provider = provider;
	}

}
