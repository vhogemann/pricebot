package com.hogemann.pricebot.model;

import java.io.Serializable;
import java.util.UUID;

public abstract class AbstractEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	private String uuid = UUID.randomUUID().toString();

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	
}
