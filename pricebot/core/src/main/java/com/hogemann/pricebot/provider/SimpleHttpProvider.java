package com.hogemann.pricebot.provider;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import com.hogemann.pricebot.Provider;
import com.hogemann.pricebot.model.Product;

public class SimpleHttpProvider<P extends Product> implements Provider<P> {

	private static Log log = LogFactory.getLog(SimpleHttpProvider.class);
	private Parser<String, List<P>> parser;
	private Parser<String[], String> urlParser;

	public List<P> find(String... ids) {
		
		String url = urlParser.parse(ids);
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);
		
		ResponseHandler<String> handler = new BasicResponseHandler();
		
		try {
			String body = client.execute(get, handler);
			return parser.parse(body);
			
		} catch (ClientProtocolException e) {
			log.error(e);
		} catch (IOException e) {
			log.error(e);
		} finally {
			client.getConnectionManager().shutdown();
		}
		
		return null;
	}
	
	public Parser<String, List<P>> getParser() {
		return parser;
	}

	public void setParser(Parser<String, List<P>> parser) {
		this.parser = parser;
	}

	public Parser<String[], String> getUrlParser() {
		return urlParser;
	}

	public void setUrlParser(Parser<String[], String> urlParser) {
		this.urlParser = urlParser;
	}

}
