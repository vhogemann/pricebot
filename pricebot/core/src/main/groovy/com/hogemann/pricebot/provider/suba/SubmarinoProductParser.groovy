package com.hogemann.pricebot.provider.suba

import org.cyberneko.html.parsers.SAXParser

import com.hogemann.pricebot.model.Product
import com.hogemann.pricebot.provider.Parser

class SubmarinoProductParser implements Parser<String, List<Product>> {
	List<Product> parse(String input){
		def html = new XmlSlurper(new SAXParser()).parseText(input);
		def ul = html.'**'.findAll{ it.'..'.'@id' == 'ul_product_list1' }
		
		return ul.collect { li ->
			
			def name = li.'**'.find{ it.'@class' == 'name entry-title' }.text()
			def brand = li.'**'.find{ it.'@class' == 'brand' }.text()
			def price = li.'**'.find{ it.'@class' == 'boxPrice' }.'**'.find{ it.'@class' == 'for'}.children()[0].text()

			Product prod = new Product()
			prod.name = name
			prod.brand = brand
			prod.price =  cleanPrice(price)
			
			return prod
		}
	}
	
	def cleanPrice( String price){
		def p = price.replace('R$ ', '').replace('.', '').replace(',', '.')
		return new BigDecimal(p)
	}
	
}
