package com.hogemann.pricebot.provider.acom

import java.util.List

import org.cyberneko.html.parsers.SAXParser

import com.hogemann.pricebot.model.Product
import com.hogemann.pricebot.provider.Parser

class AmericanasProductParser implements Parser<String,List<Product>>{
	
	List<Product> parse(String input){
		def html = new XmlSlurper(new SAXParser()).parseText(input);
		def ul = html.'**'.findAll{ it.'..'.'@class' == 'pList' }
		
		return ul.collect { li ->
			
			if( li.'@class' != 'lfix li'){

				def name = li.'**'.find{ it.'@class' == 'n name fn' }.text()
				def brand = li.'**'.find{ it.'@rel' == 'brand' }.text()
				def price = li.'**'.find{ it.'@class' == 'sale price' }.text()
		
				Product prod = new Product()
				prod.name = name
				prod.brand = brand.replace('+ ', '')
				prod.price =  cleanPrice(price)
				
				return prod
			}
		}		
	}
	
	def cleanPrice( String price){
		def p = price.replace('Por: ', '').replace('R$ ', '').replace('.', '').replace(',', '.')
		return new BigDecimal(p)
	}
}
