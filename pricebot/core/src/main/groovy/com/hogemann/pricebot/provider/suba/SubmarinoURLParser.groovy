package com.hogemann.pricebot.provider.suba

import com.hogemann.pricebot.provider.Parser;

class SubmarinoURLParser implements Parser<String[],String> {
	
	String parse(String[] input){
	
		def query = ''
		input.each { item ->
			query = query + item + ' '
		}
		query = query.trim()
		def url = "http://www.submarino.com.br/busca/?q=${query}"
		return url
		
	}
	
}
