package com.hogemann.pricebot.provider.acom

import com.hogemann.pricebot.provider.Parser;

class AmericanasURLParser implements Parser<String[], String> {

	String parse(String[] input){
		
			def query = ''
			input.each { item ->
				query = query + item + ' '
			}
			query = query.trim()
			def url = "http://www.americanas.com.br/busca/${query}"
			return url
			
		}
	
}
