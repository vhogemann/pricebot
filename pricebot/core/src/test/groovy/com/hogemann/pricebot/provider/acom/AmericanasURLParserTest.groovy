package com.hogemann.pricebot.provider.acom

import org.junit.Test;

class AmericanasURLParserTest {

	@Test
	def void parseTest(){
		
		def parser = new AmericanasURLParser()
		
		String[] ids = ['123','456'].toArray()
		
		def expected = "http://www.americanas.com.br/busca/123 456"
		def actual = parser.parse(ids)
		
		assert actual == expected
	}
	
}
