package com.hogemann.pricebot.provider.acom

import org.junit.Test;

class AmericanasProductParserTest {
	
	@Test
	def void parseTest(){
		def file = new File("src/test/resources/americanas_busca.xhtml")
		def doc = ''
		file.eachLine{ doc += it }
		def parser = new AmericanasProductParser()
		def list = parser.parse(doc)
		
		assert !list.empty
		
		assert list.size() == 2
		
		assert list[0].name == 'Smartphone Nokia N8 Cinza - GSM c/ Sistema Operacional Symbian 3, Tecnologia 3G, Wi-Fi, TouchScreen, GPS, Câmera 12MP c/ lente Carl Zeiss c/ Flash Xenon e Câmera p/ Chamadas de Video, Filmadora HD c/ Saída HDMI e Som Dolby Digital, MP3 Player, Rádio FM, Bluetooth Estéreo 3.0, Cabo de Dados, Fone e Memória interna de 16GB'
		assert list[0].brand == 'Nokia'
		assert list[0].price == 999.00
		
		assert list[1].name == 'Smartphone Nokia N9 Preto 3G Wi-fi OviMapas NFC Câm 8MP Vídeo HD MP3 16GB'
		assert list[1].brand == 'Nokia'
		assert list[1].price == 1699.00
	}
	

}
