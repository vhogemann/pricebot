package com.hogemann.pricebot.provider.suba

import org.junit.Test;

import com.hogemann.pricebot.provider.suba.SubmarinoProductParser;

class SubmarinoProductParserTest {
	
	@Test
	def void parseTest(){
		def file = new File("src/test/resources/submarino_busca.xhtml")
		def doc = ''
		file.eachLine{ doc += it }
		def parser = new SubmarinoProductParser()
		def list = parser.parse(doc)
		
		assert !list.empty
		
		assert list.size() == 2
		
		assert list[0].name == 'TV 42" LCD Full HD Stile D42 c/ Conversor Digital HDMI e USB - CCE'
		assert list[0].brand == 'CCE'
		assert list[0].price == 1799.00
		
		assert list[1].name == 'TV LCD 42 Com Receptor Digital Integrado - AOC'
		assert list[1].brand == 'AOC'
		assert list[1].price == 1899.00
	}
	
}