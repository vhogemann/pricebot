package com.hogemann.pricebot.provider


import org.junit.Test;

import com.hogemann.pricebot.model.Product
import com.hogemann.pricebot.provider.acom.AmericanasProductParser;
import com.hogemann.pricebot.provider.acom.AmericanasURLParser;
import com.hogemann.pricebot.provider.suba.SubmarinoProductParser
import com.hogemann.pricebot.provider.suba.SubmarinoURLParser

class SimpleHttpProviderTest {

	@Test
	def void findSubaTest(){
		
		SimpleHttpProvider<Product> provider = new SimpleHttpProvider<Product>()
		
		provider.setParser(new SubmarinoProductParser())
		provider.setUrlParser(new SubmarinoURLParser())
		
		def products = provider.find('000')
		
		assert products == null
		
		products = provider.find('23769882')
		
		assert products != null
		assert !products.empty
		
	}
	
	@Test
	def void findAcomTest(){
		SimpleHttpProvider<Product> provider = new SimpleHttpProvider<Product>()
		
		provider.setParser(new AmericanasProductParser())
		provider.setUrlParser(new AmericanasURLParser())
		
		def products = provider.find('000')
		
		assert products == null
		
	}
	
	
}
