package com.hogemann.pricebot.provider.suba

import org.junit.Test

import com.hogemann.pricebot.provider.suba.SubmarinoURLParser;

class SubmarinoURLParserTest {
	
	@Test
	def void parseTest(){
		
		def parser = new SubmarinoURLParser()
		
		String[] ids = ['123','456'].toArray()
		
		def expected = "http://www.submarino.com.br/busca/?q=123 456"
		def actual = parser.parse(ids)
		
		assert actual == expected
	}
	
}
